# Nubonetics Development Environment

Nubonetics Development Environment (NDE) is a modified version of [ADE Development Environment](https://ade-cli.readthedocs.io/en/latest/). Docker images made with NDE are ADE compatible. They can also be used without ADE. There are two modes of using NDE:

- $USER: This mode is ADE compatible. At the entrypoint a user with the same name and User ID (UID) as the user who is creating the container is built inside the container. In this mode, it is assumed that these variables are passed to the container: `$USER`, `$USER_ID`, `$GROUP`, `$GROUP_ID` and `${VIDEO_GROUP_ID}`.
- root: In this mode, no new user is created in the container. 
